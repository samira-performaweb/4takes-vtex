import 'select2';
import 'slick-carousel';

const $win = $(window);
const $doc = $(document);

$('.slider-intro .slider__slides').slick({
	dots: true,
	arrows: true,

	responsive: [{
		breakpoint: 1024,
		settings: {
			arrows: false,
		}
	}]
});

if($('.slider-logos').length){
	$('.slider-logos .slider__slides').each(function(){
		$(this).slick({
			slidesToShow: 6,
			slidesToScroll: 6,

			responsive: [{
				breakpoint: 1300,
				settings: {
					slidesToShow: 5,
					slidesToScroll: 5,
				}
			},
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: false,
  					variableWidth: true,
					arrows: false,
					dots: true
				}
			}]
		});
	});
}

$('.slider-products .slider__slides').slick({
	draggable: false,
	slidesToShow: 4,
	slidesToScroll: 4,
	arrows: true,

	responsive: [{
		breakpoint: 1024,
		settings: {
			draggable: true,
			slidesToShow: 2,
			slidesToScroll: 2,
			dots: true,
			arrows: false,
		}
	}]
});

$('.slider-products .slider__slides').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	isSliding = true;

	setTimeout(function () {
		isSliding = false;
	}, 400)
});

$('.slider-gallery .slider__slides').slick({
	slidesToShow: 1,
	slidesToScroll: 1,

	responsive: [{
		breakpoint: 1024,
		settings: {
			dots: true,
			arrows: false,
		}
	}]
});

$('.tabs .tabs__nav a').on('click', function(event){
	event.preventDefault();
	const $this = $(this);
	const $target = $($this.attr('href'));

	$this.parent().addClass('current').siblings().removeClass('current');
	$target.addClass('active').siblings().removeClass('active')
})

let $tempProduct;
let elContent;
let isSliding = false;

if($win.width() > 1023){
	$('.products .product').on('mouseenter', function(){
		if ( isSliding == false ) {
			const $this = $(this);
			const $target = $('.js-popup-product');
			const elWidth = $this.width();
			const elpos = $this.offset();

			$tempProduct = $this;
			elContent = $this.html();

			$target
				.stop()
				.show()
				.width(elWidth)
				.css({top: elpos.top, left: elpos.left-7})
				.html(elContent);
			// $this.html(' ');

			const $targetActions = $target.find('.product__actions');

			$targetActions.stop();
			$targetActions.slideDown(200);
		}
	});

	$('.js-popup-product').on('mouseleave', function(){
		if ( isSliding == false ) {
			const $this = $(this);

			$this.hide();
			$this.find('.product__actions').stop().slideUp(200);

			$tempProduct.html(elContent);
		}
	});
}

$('.widget .widget__toggle').on('click', function(event){
	event.preventDefault();

	const $target = $(this).closest('.widget');

	$target.toggleClass('active');
	$target.find('.widget__content').slideToggle();
});

if($win.width() < 1024){
	$('.gallery-product .gallery__images').slick({
		dots: true,
		arrows: true,

		responsive: [{
			breakpoint: 1024,
			settings: {
				arrows: false,
			}
		}]
	});

	$('.nav-utilities .has-dropdown > a').on('click', function(event){
		event.preventDefault();

		const $this = $(this).parent();

		if(!$this.hasClass('active')){
			$this.addClass('active').removeClass('disabled').siblings().addClass('disabled').removeClass('active');
			$('.btn-menu').addClass('disabled');
			$('.nav').removeClass('active');
		}
		else {
			$('.has-dropdown').removeClass('active').removeClass('disabled');
	        $('.btn-menu').removeClass('disabled');
		}
	});

	$('.dropdown').on('click', function(event) {
	    if ( $(event.target).closest('.dropdown__inner').length === 0 ) {
	        $('.has-dropdown').removeClass('active').removeClass('disabled');
	        $('.btn-menu').removeClass('disabled');
	    }
	});

	$('.btn-menu').on('click', function(event){
		event.preventDefault();

		const $this = $(this);

		if(!$this.hasClass('active')){
			$this.removeClass('disabled').addClass('active');

			$('.nav').addClass('active');
			$('.nav-utilities .has-dropdown').addClass('disabled');
		}

		else {
			$this.removeClass('active').removeClass('disabled');

			$('.nav').removeClass('active');
			$('.nav-utilities .has-dropdown').removeClass('disabled');
		}

		$('.has-dropdown').removeClass('active');
	});

	$('.nav .has-dropdown > a').on('click', function(event){
		event.preventDefault();

		const $this = $(this).parent();

		$this.addClass('active');
	});

	$('.js-close-dropdown').on('click', function(event) {
		event.preventDefault();

		$(this).closest('.has-dropdown').removeClass('active');
	});

	$('.js-show-filter').on('click', function(event) {
		event.preventDefault();

		$('#filter').toggleClass('active');
		$(this).toggleClass('active');
	});

	$('.js-hide-filter').on('click', function(event) {
		event.preventDefault();

		$('#filter').removeClass('active');
		$('.js-show-filter').removeClass('active');
	});

	$('.footer .footer__title').on('click', function(event){
		event.preventDefault();

		$(this).find('.footer__icon').toggleClass('active');
		$(this).parent().children('ul').slideToggle();
	});

	$('.tab .tab__head').on('click', function(event){
		event.preventDefault();

		$(this).toggleClass('active').siblings('.tab__entry').slideToggle();
	})
}

$('.gallery-product .gallery__image').on('click', function(event){
	event.preventDefault();

	const $this = $(this);
	const imageSrc = $this.attr('href');

	$this.addClass('active');
	$this.parent().siblings().children().removeClass('active');

	$('.gallery-product .gallery__image-large').children().attr('src', imageSrc);
})


function selectTemplate (state) {
	if (!state.id) {
		return state.text;
	}

	let $state = $(
		'<span class="circle" style="background-color: ' + state.id + '"></span>' + '<span>' + state.text + '</span>'
	);

	return $state;
};

const selectedTemplate = ($selector, value, text) => {
	const selectedTemplate = `
   		<span class="circle" style="background-color: ${value}"></span>

   		<span>${text}</span>
   	`;

    $selector.html(selectedTemplate);
}

$('.select--small select').select2({
	minimumResultsForSearch: -1,
	templateResult: selectTemplate
}).each(function() {
	selectedTemplate(
		$(this).parent().find('.select2-selection__rendered'),
		this.value,
		$(this).find(`option[value="${this.value}"]`).text()
	);
});

$('.select--small select').on('select2:select', function (e) {
    const data = e.params.data;

    selectedTemplate(
    	$(this).parent().find('.select2-selection__rendered'),
    	data.id,
    	data.text
	);
});

$('.tab .tab__more').on('click', function(event){
	event.preventDefault();

	$(this).fadeOut().closest('.tab__entry').addClass('active')
});
